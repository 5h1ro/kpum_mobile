import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:get/get.dart';
import 'package:kpum_mobile/controller/loginController.dart';
import 'package:kpum_mobile/page/home.dart';
import 'package:kpum_mobile/page/login.dart';
import 'package:kpum_mobile/route/pages.dart';
import 'package:kpum_mobile/theme/theme.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final LoginController loginController = Get.put(LoginController());
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(builder: (context, snapshot) {
      return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        getPages: AppRoutes.pages,
        theme: appTheme(),
        home: snapshot.data != null ? HomePage() : LoginPage(),
        defaultTransition: Transition.cupertino,
      );
    });
  }
}
