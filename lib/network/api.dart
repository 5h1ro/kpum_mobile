import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Api {
  final String _url = 'http://192.168.13.134:8000/api/';
  var token;

  _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var todoString = localStorage.getString('token');
    if (todoString != null) {
      token = jsonDecode(todoString)['token'];
    }
  }

  auth(data, url) async {
    var fullUrl = _url + url;
    return await http.post(Uri.parse(fullUrl), body: data);
  }

  // getData(apiURL) async {
  //   var fullUrl = _url + apiURL;
  //   await _getToken();
  //   return await http.get(
  //     fullUrl,
  //     headers: _setHeaders(),
  //   );
  // }

  _setHeaders() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };
}
