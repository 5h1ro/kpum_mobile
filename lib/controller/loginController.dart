import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kpum_mobile/network/api.dart';

class LoginController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final npm = TextEditingController();
  final password = TextEditingController();

  var onShow = true.obs;

  void showPassword() {
    this.onShow.value = !this.onShow.value;
  }

  Future<void> pressLogin() async {
    Get.focusScope?.unfocus();
    var data = {'npm': npm.text, 'password': password.text};
    if (npm.text == '' || password.text == '') {
      if (npm.text == '') {
        Get.snackbar('Error', 'NPM belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (password.text == '') {
        Get.snackbar('Error', 'Password belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else {
        Get.snackbar('Error', 'NPM dan Password belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    } else {
      var res = await Api().auth(data, 'login');
      var body = json.decode(res.body);
      if (body['success']) {
        Get.snackbar('Success', 'Login Berhasil',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else {
        Get.snackbar('Error', 'NPM atau Password Salah',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    }
  }
}
