import 'package:get/get.dart';
import 'package:kpum_mobile/page/login.dart';

part 'routes.dart';

class AppPages {
  static const Login = _Paths.Login;
}

abstract class _Paths {
  static const Login = '/login';
}
